import React, { useState } from "react";

export const servers = [
  {
    _id: "1",
    serverName: "Dokku User Server ",
    description: "Client and Admin server base still on preview",
    active: false,
  },
  {
    _id: "2",
    serverName: "Over Flow Server",
    description: "Client and Admin server base still on preview",
    active: false,
  },
  {
    _id: "3",
    serverName: "Deep tech Server",
    description: "Client and Admin server base still on preview",
    active: false,
  },

  {
    _id: "4",
    serverName: "Viz Dev Server",
    description: "Client and Admin server base still on preview",
    active: false,
  },
  {
    _id: "5",
    serverName: "Admin Server",
    serveractiveption: "Client and Admin server base still on preview",
    active: false,
  },
  {
    _id: "6",
    serverName: "H2H Server",
    description: "Client and Admin server base still on preview",
    active: false,
  },
  {
    _id: "7",
    serverName: "Hands 2 hold Server",
    description: "Client and Admin server base still on preview",
    active: false,
  },
];

export const getSpecifiedServer = ({ server, id, setServer }) => {
  const foundServer = server.find((val) => val._id === id);
  setServer(() => foundServer);
};
