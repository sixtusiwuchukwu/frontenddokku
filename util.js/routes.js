let serverUser = "Emma";
export const UserRoutes = [
  {
    items: [
      {
        label: "Overview",
        icon: "pi pi-fw pi-share-alt",
        to: `/userDash/${serverUser}/overview`,
      },

      {
        label: "Repo",
        icon: "pi pi-fw  pi-cloud",
        to: `/userDash/${serverUser}/repo`,
      },
      {
        label: "Analytics View",
        icon: "pi pi-fw  pi-sitemap",
      },

      {
        label: "Milistones",
        icon: "pi pi-fw  pi-sitemap",
      },

      {
        label: "Activity",
        icon: "pi pi-fw pi-users",
        to: "",
      },
      {
        label: "Settings",
        icon: "pi pi-fw pi-cog",
      },

      {
        label: "Solve an issue",
        icon: "pi pi-fw pi-exclamation-circle",
      },
      {
        label: "Report a problem",
        icon: "pi pi-fw   pi-comments",
      },
      {
        label: "Support",
        icon: "pi pi-fw  pi-question-circle",
      },
      {
        label: "Policies",
        icon: "pi pi-fw  pi-external-link",
      },
      {
        label: "user Validity",
        icon: "pi pi-fw  pi-shield",
      },
    ],
  },
];
