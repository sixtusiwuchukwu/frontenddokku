import { ForgotPasword } from "./styled.Forgot";
import React, {useState} from "react";
import Image from "next/image";
import logo from "../../assete/logo.png";
import CustomForm from "../CustomForm/Form";
import * as Yup from "yup";
import TextInput from "../Input/TextInput";
import {gql, useMutation} from "@apollo/client";
import {FiMail} from "react-icons/fi";

const initialSchemaAccountInformation = Yup.object().shape({
  email: Yup.string().required().email("Invalid email address. E.g. example@email.com")
});
const Inputs = [
  {
    placeholder: 'Email*',
    name: 'email',
    inputType: 'email',
    as: TextInput,
    icon:"pi-envelope"
  }
];
const forgotPassword = gql`
    mutation forgotPassword($email: EmailAddress!) {
        forgotPassword(email: $email)
    }
`
const defaultData = {
  email: ''
};

const ForGotPasswordResponse = ({email}) => {
  return (
    <>
      <Image height={40} width={30} src={logo} className="login-logo" />
      <h4>DPASM PASSWORD RESET</h4>
      <div><FiMail size={100}/></div>
      <div>An email have been sent to <b>{email}</b>, please go to your mail box to complete password reset</div>
    </>)
}

const ForgotPassword = () => {
  const [isCompleted, setIsCompleted] = useState(false)
  const [email, setEmail] = useState("")
  const [requestReset, {loading}] = useMutation(forgotPassword, {
    onCompleted: ()=>setIsCompleted(true)
  })
  return (
    <ForgotPasword>
      <div className="form-auth">
        <div className="p-d-flex p-jc-center">
          <div className="card">
            {!isCompleted ? <> <div className="logo-container">
                <div className="login-logo-wrapper">
                <Image src={logo} className="login-logo" />
              </div>
            </div>
             <h2 className="p-text-center-dash" style={{ fontSize: "18px" }}>
                Reset your password
              </h2>
              <p
                style={{
                  fontWeight: "600",
                  color: "grey",
                  padding: "2px",
                  fontSize: "12px",
                }}
              >
                Enter your user account's verified email address
                <br />
                and we will send you a password reset link.
              </p>
            <CustomForm
              alignItems="flex-end !important"
              inputWidth="100%"
              onSubmitFunction={async (fields) => {
                setEmail(fields.email)
                requestReset({variables: fields}).catch(() => {
                })
              }}
              initialSchema={initialSchemaAccountInformation}
              initialValues={defaultData}
              title=""
              infoText=""
              btnText={loading ? 'please wait' : 'Reset Password'}
              loading={loading}
              formInputs={Inputs}
            /></>: <ForGotPasswordResponse email={email}/>}
          </div>
        </div>
      </div>
    </ForgotPasword>
  );
};

export default ForgotPassword;
