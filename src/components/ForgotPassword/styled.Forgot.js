import styled from "styled-components";

export const ForgotPasword = styled.div`
    margin:1rem;
h2.p-text-center-dash {
    text-align: center;
    font-weight: 600;
    /* margin: 0; */
  }

  .form-auth {
    display: flex;
    align-items: center;
    flex: 1;
    justify-content: center;
    height: 100%;
    margin-top:6rem;
  }
  .form-auth .card {
    min-width: 350px;
    justify-content: center;
    text-Align:center;
  }
  .logo-container {
    justify-content: center;
    width: 100%;
    display: flex;
    position: relative;
    .login-logo-wrapper {
      width: 13%;
      margin: 10px;
      .login-logo {
        width: 100%;
        height: 100%;
      }
    }
  }

  .card .p-fluid .p-field {
    padding: 10px;
  }
  .form-auth .card form {
    /* margin-top: 3rem; */
  }
  .form-auth .card .p-field {
    margin-bottom: 2rem;
  }

  .p-mt-2 {
    padding: 13px;
    font-size: 14px;
    font-weight: 600;
    margin-bottom: 15px;
  }
  .Login-wrapper {
    display: flex;
    justify-content: space-between;
    flex-wrap: wrap;
    cursor: pointer;
    position: relative;
    padding: 0;

    .Login-text-wrapper {
      list-style-type: none;
      font-size: 12px;
      font-weight: 500;

      Link {
        padding-left: 30px;
      }
    }
  }

  @media screen and (max-width: 960px) {
    .form-auth .card {
      width: 80%;
    }
  }
  @media screen and (max-width: 640px) {
    .form-auth .card {
      width: 100%;
      min-width: 0;
    }
  }

  .area {
    background: #4e54c8;
    background: -webkit-linear-gradient(to left, #8f94fb, #4e54c8);
    width: 100%;
  /
  }
  .circles {
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    position: absolute;
    overflow: hidden;
  }
  .circles .circle-box {
    position: absolute;
    display: block;
    list-style: none;
    width: 100%;
    height: 50px;
    background: rgba(255, 255, 255, 0.2);
    animation: animate 25s linear infinite;
    bottom: -150px;
    z-index: -999;
  }

  .circles .circle-box:nth-child(1) {
    left: 25%;
    width: 50px;
    height: 50px;
    animation-delay: 0s;
  }

  .circles .circle-box:nth-child(2) {
    left: 10%;
    width: 50px;
    height: 50px;
    animation-delay: 2s;
    animation-duration: 12s;
  }

  .circles .circle-box:nth-child(3) {
    left: 70%;
    width: 50px;
    height: 50px;
    animation-delay: 4s;
  }

  .circles .circle-box:nth-child(4) {
    left: 40%;
    width: 50px;
    height: 50px;
    animation-delay: 0s;
    animation-duration: 18s;
  }

  .circles .circle-box:nth-child(5) {
    left: 65%;
    width: 50px;
    height: 50px;
    animation-delay: 0s;
  }

  .circles .circle-box:nth-child(6) {
    left: 75%;
    width: 50px;
    height: 50px;
    animation-delay: 3s;
  }

  .circles .circle-box:nth-child(7) {
    left: 35%;
    width: 50px;
    height: 50px;
    animation-delay: 7s;
  }

  .circles .circle-box:nth-child(8) {
    left: 50%;
    width: 50px;
    height: 50px;
    animation-delay: 15s;
    animation-duration: 45s;
  }

  .circles .circle-box:nth-child(9) {
    left: 20%;
    width: 50px;
    height: 50px;
    animation-delay: 2s;
    animation-duration: 35s;
  }

  .circles .circle-box:nth-child(10) {
    left: 85%;
    width: 50px;
    height: 50px;
    animation-delay: 0s;
    animation-duration: 11s;
  }

  @keyframes animate {
    0% {
      transform: translateY(0) rotate(0deg);
      opacity: 1;
      border-radius: 0;
    }

    100% {
      transform: translateY(-1000px) rotate(720deg);
      opacity: 0;
      border-radius: 50%;
    }
  }
`;
