import React from "react";
import { Header } from "./styled.header";
// import image
import Image from "next/image";
import logo from "../../../assete/logo.png";
import left from "../../../assete/landingpage/left.svg";
import right from "../../../assete/landingpage/right.svg";

const PageHeader = () => {
  return (
    <Header className="">
      <div className="Header-container">
        <div className="Container">
          <div className="blank-header">
            <Image src={left} className="image-border image-border-left" />
          </div>

          <div className="content-center brand">
            <h1 className="content-title">
              Simplify your workflow with <br /> Dokku server
            </h1>
            <h3 className="content-sub-title">
              A Design System for Server Management
              <br />
              with Open source doc.
            </h3>
          </div>

          <div className="blank-header">
            <Image src={right} className="image-border image-border-right" />
          </div>
        </div>
      </div>
    </Header>
  );
};

export default PageHeader;
