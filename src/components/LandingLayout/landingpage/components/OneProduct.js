import React from "react";
import { Oneproduct } from "./styled.OneProduct";

const OneProduct = () => {
  return (
    <Oneproduct className="surface-tab text-center">
      <div className="mb-3 font-bold text-2xl">
        <span className="text-600">
          A powerful and innovative feature set,{" "}
        </span>
        <span className="text-blue-500">Dokku Runtime</span>
      </div>
      <div className="text-900 line-height-3 font-bold text-1xl ">
        Built for developers, by developers.
      </div>

      <div className="grid">
        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-desktop text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Dokku Postgres (SQL)</div>
          <span className="text-900 line-height-3">
            Reliable and secure PostgreSQL as a service with easy setup,
            encryption at rest, simple scaling, database forking, continuous
            protection, and more.
          </span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-lock text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Dokku Integration</div>
          <span className="text-900 line-height-3">
            Our seamless GitHub integration means every pull request spins up a
            disposable Review App for testing, and any repo can be set up to
            auto-deploy with every GitHub push to a branch of your choosing.
          </span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-check-circle text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Extensibility</div>
          <span className="text-900 line-height-3">
            Customize your stack with a Dokku innovation: Buildpacks. Build your
            own, or choose one from the hundreds built by the community to run
            Gradle, Meteor, NGINX — even Haskell.
          </span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-globe text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Continuous delivery</div>
          <span className="text-900 line-height-3">
            Dokku Flow uses Dokku Pipelines, Review Apps and GitHub Integration
            to make building, iterating, staging, and shipping apps easy,
            visual, and efficient.......
          </span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-github text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">
            Code and data rollback
          </div>
          <span className="text-900 line-height-3">
            Work fearlessly — Heroku’s build system and Postgres service let you
            roll back your code or your database to a previous state in an
            instant.
          </span>
        </div>

        <div className="col-12 md:col-4 md:mb-4 mb-0 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-shield text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">App metrics</div>
          <span className="text-900 line-height-3">
            Always know what’s going on with your apps thanks to built-in
            monitoring of throughput, response times, memory, CPU load, and
            errors.
          </span>
        </div>
        {/* ----------------------- */}
        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-desktop text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Dokku Runtime</div>
          <span className="text-900 line-height-3">
            Your apps run inside smart containers in a fully managed runtime
            environment, we handle everything critical for production —
            configuration, orchestration, load balancing, failovers, logging,
            security, and more
          </span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-lock text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Dokku Postgres (SQL)</div>
          <span className="text-900 line-height-3">
            Reliable and secure PostgreSQL as a service with easy setup,
            encryption at rest, simple scaling, database forking, continuous
            protection, and more.
          </span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-check-circle text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Dokku Redis</div>
          <span className="text-900 line-height-3">
            The most popular in-memory, key-value datastore — delivered as a
            service. Dokku Redis provides powerful data types, great throughput,
            and built-in support for top languages.
          </span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-globe text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Scale</div>
          <span className="text-900 line-height-3">
            Dokku scales in an instant, both vertically and horizontally. You
            can elegantly run everything from tiny hobby projects to
            enterprise-grade e-commerce handling Black Friday surges.......
          </span>
        </div>

        <div className="col-12 md:col-4 mb-4 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-github text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Add-ons</div>
          <span className="text-900 line-height-3">
            Extend, enhance, and manage your applications with pre-integrated
            services like New Relic, MongoDB, SendGrid, Searchify, Fastly,
            Papertrail, ClearDB MySQL, Treasure Data, and more.
          </span>
        </div>

        <div className="col-12 md:col-4 md:mb-4 mb-0 px-3">
          <span
            className="p-3 shadow-2 mb-3 inline-block"
            style={{ borderRadius: "10px" }}
          >
            <i className="pi pi-shield text-4xl text-blue-500"></i>
          </span>
          <div className="text-900 mb-3 font-medium">Data Clips</div>
          <span className="text-900 line-height-3">
            Data Clips make it easy to keep everyone in the loop with
            up-to-the-second data insights from your project by sharing query
            results via a simple and secure URL.
          </span>
        </div>
      </div>
    </Oneproduct>
  );
};

export default OneProduct;
