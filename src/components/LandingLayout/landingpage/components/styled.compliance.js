import styled from "styled-components";

export const Achievement = styled.div`
  padding: 30px;
  background: #fff;
  /* box-shadow: 2px 2px 2px 1px rgba(5, 5, 5, 5.2); */
  /* border-radius: 10px; */
  cursor: pointer;
  text-align: center;
  /* background-color: -webkit-linear-gradient(-70deg, #9867f0 0%, #ed4e50 100%); */

  .text-center {
    border-radius: 10px;
    padding: 5px;
  }
  h3 {
    color: black;
    font-size: 14px;
    font-weight: 600;
  }
  span {
    color: black;
    font-size: 13px;
  }
  h4 {
    font-weight: 600;
    font-size: 12px;
    color: black;
  }
  .white {
    background: white;
  }
`;
