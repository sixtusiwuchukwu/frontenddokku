import styled from "styled-components";

export const Hometabs = styled.div`
  padding: 5px;
  position: relative;
  flex-wrap: wrap;
  justify-content: center;
  justify-content: space-around;
  /* height: 100%; */
  position: relative;
  background-color: rgb(255, 255, 255);
  color: rgb(17, 37, 69);

  .tabs {
    margin: auto;
    display: flex;
    flex-wrap: wrap;
    margin: 48px;
    justify-content: space-around;
  }
  .landingpage-img {
    border-radius: 7px;
  }

  .cards {
    font-weight: 600;
    box-shadow: 15px 18px 71px 10px solid grey;
  }
  .cards p {
    font-size: 12px;
    font-weight: 500;
  }
  .cards h6 {
    font-size: 12px;
    font-weight: 600;
  }

  .surface {
    background-color: white;
    border-radius: 12px;
    box-shadow: 2px 2px 2px 1px rgba(0, 0, 0, 0.2);
    padding: 25px;
  }

  /* Absolute div */

  .blank-header + .manage-content-container {
    /* margin-bottom: -127px; */
    position: absolute;
  }
  .manage-content-container {
    background: #f9f9f9;
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.1);
    border-radius: 6px;
    color: #333;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    /* justify-content: stretch; */
    margin: 0 auto;
    margin-bottom: 0px;
    max-width: 500px;
    text-decoration: none;
    transform: translateY(-80px);
    transition: 0.2s cubic-bezier(0, 0, 0, 1);
    width: calc(100% - 20px);
    /* position: relative; */
  }

  .manage-image {
    background-position: 0 0;
    background-size: cover;
    flex: 0 1 auto;
    overflow: hidden;
    /* border-radius: 6px 0px 0px 6px; */
    border: solid #d3d3d3;
    border-top-width: medium;
    border-right-width: medium;
    border-bottom-width: medium;
    border-left-width: medium;
    border-width: 0 1px 0 0;
    max-width: 200px;
    width: 100%;
  }

  .manage-copy-wrapper {
    flex: 0 1 auto;
    /* min-height: 220px; */
    overflow: hidden;
    padding: 12px 20px;
  }

  .manage-copy-wrapper .title h4 {
    display: block;
    font-weight: 600;
    font-size: 13px;
    width: 100%;
  }

  .manage-content-container .content p {
    display: block;
    width: 100%;
    font-size: 11px;
  }

  .manage-content-container .cta-wrapper {
    display: block;
    width: 100%;
    color: purple;
  }
  @media screen and (max-width: 960px) {
    .manage-content-container {
      width: 100%;
      min-width: 0;
      position: relative;
    }
  }
  @media screen and (max-width: 640px) {
    .manage-content-container {
      /* width: 80%;
      min-width: 0; */
      display: none;
      /* position: relative; */
    }
  }
`;
