import styled from "styled-components";

export const DropdownMenu = styled.div`
  background: white;
  text-align: start;
  position: absolute;
  top: 65px;
  width: 25%;
  text-align: start;
  margin: 0;
  padding: 5px;

  .dropdown-menu {
    /* top: 85px; */
    list-style: none;
    margin: 0;
    padding-left: 0;
    /* position: absolute; */
  }

  .dropdown-list {
    cursor: pointer;
    font-size: 12px;
    /* width: 100%; */
    padding: 5px;
    font-weight: 500;
    opacity: 1;
  }

  /* .dropdown-menu li:hover {
    background: #e0e0e0;
    color: black !important;
  } */

  .dropdown-menu.clicked {
    display: none;
  }

  .dropdown-link {
    display: block;
    height: 100%;
    width: 100%;
    text-decoration: none;
    /* margin: 10px; */
  }

  @media screen and (max-width: 960px) {
    .fa-caret-down {
      display: none;
    }
  }
`;
