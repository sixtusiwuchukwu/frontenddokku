import React, { useState, useEffect } from "react";
import { Button } from "primereact/button";
import { Nav } from "./styled.navbar";
import Link from "next/link";
import Image from "next/image";
import logo from "../../../assete/logo.png";
import { IconContext } from "react-icons/lib";
import { MdFingerprint } from "react-icons/md";
import { FaBars, FaTimes } from "react-icons/fa";
// import { logo } from "../../../assete/logo.png";
import Dropdown from "./Dropdown";

const Navbar = () => {
  const [click, setClick] = useState(false);
  const [dropdown, setDropdown] = useState(false);
  const [button, setButton] = useState(true);

  const handleClick = () => setClick(!click);
  const closeMobileMenu = () => setClick(false);

  const onMouseEnter = () => {
    if (window.innerWidth < 960) {
      setDropdown(false);
    } else {
      setDropdown(true);
    }
  };

  const onMouseLeave = () => {
    if (window.innerWidth < 960) {
      setDropdown(false);
    } else {
      setDropdown(false);
    }
  };
  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };

  return (
    <>
      <IconContext.Provider value={{ color: "#fff" }}>
        <Nav className="navbar">
          <div className="navbar-container container">
            <div className="logo-container" onClick={closeMobileMenu}>
              <div className="login-logo-wrapper">
                <Image src={logo} className="login-logo" />
              </div>
            </div>

            <div className="menu-icon" onClick={handleClick}>
              {click ? <FaTimes /> : <FaBars />}
            </div>

            <ul className={click ? "nav-menu active" : "nav-menu"}>
              <li className="nav-item">
                <Link href="/" onClick={closeMobileMenu} className="nav-links">
                  Home
                </Link>
              </li>
              <li
                className="nav-item"
                onMouseEnter={onMouseEnter}
                onMouseLeave={onMouseLeave}
              >
                <Link
                  href="/services"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Service
                </Link>

                <i className="pi pi-chevron-down"></i>
                {dropdown && <Dropdown />}
              </li>

              <li className="nav-item">
                <Link
                  href="/product"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Product
                </Link>
                <i className="pi pi-chevron-down"></i>
              </li>
              <li className="nav-item">
                <Link
                  href="/userdashboard"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Explore
                </Link>
                <i className="pi pi-chevron-down"></i>
              </li>

              <li className="nav-item">
                <Link
                  href="/signup"
                  className="nav-links"
                  onClick={closeMobileMenu}
                >
                  Sign Up
                </Link>
              </li>

              <li className="nav-item">
                <Link href="/login" className="login-btn">
                  Login
                </Link>
              </li>
            </ul>
          </div>
        </Nav>
      </IconContext.Provider>
    </>
  );
};

export default Navbar;
