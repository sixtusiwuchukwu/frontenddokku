export const MenuItems = [
  {
    title: "GitLab: the DevOps platform →",
    text: "Explore GitLab",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Discover all-in-one software delivery →",
    text: "Install GitLab ",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Install one package, run a complete solution →",
    text: "How GitLab compares ",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "See repositories in action with GitLab projects →",
    text: "See why we're the best in DevOps",
    path: "/development",
    cName: "dropdown-link",
  },
];
