import React from 'react'
import { Footerpage } from './styled.Footer'
import {favicon} from '../../../../public/favicon.ico'

const Footer = () => {
    return (
        <Footerpage>
            
            <div className="logo-img">
                
                <img src="/favicon.ico" alt='favicon' style={{width:"60px",margin:"30px"}}/>

            </div>

        <div className="tabs flex"> 

     
     

        <div className="cards" style={{  marginBottom: '2em' }}>
              
             <ul className="list-none">
             <h2>Why Dokku?</h2>
          <li className="flex align-items-center mb-4">
              <i className="pi pi-check-circle text-green-500 mr-2"></i>
              <span>product</span>
          </li>
          <li className="flex align-items-center mb-4">
              <i className="pi pi-check-circle text-green-500 mr-2"></i>
              <span>solution</span>
          </li>
          <li className="flex align-items-center mb-4">
              <i className="pi pi-check-circle text-green-500 mr-2"></i>
              <span>services</span>
          </li>
          <li className="flex align-items-center mb-4">
              <i className="pi pi-check-circle text-green-500 mr-2"></i>
              <span>DevOps tools</span>
          </li>
          <li className="flex align-items-center mb-4">
          <i className="pi pi-check-circle text-green-500 mr-2"></i>
          <span>Is It any good?</span>
            </li>
            <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Releases</span>
                </li>
                <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Pricing</span>
                </li>
                <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Get Started</span>
                </li>
             </ul>
        </div>

        <div className="cards" style={{  marginBottom: '2em' }}>
            <ul className="list-none">
                <h2>Resources</h2>
            
        <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>All resources</span>
        </li>
        <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>All-Remote</span>
        </li>
        <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Blog</span>
        </li>
        <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Newsletter</span>
        </li>
        <li className="flex align-items-center mb-4">
        <i className="pi pi-check-circle text-green-500 mr-2"></i>
        <span>Event</span>
        </li>
        <li className="flex align-items-center mb-4">
        <i className="pi pi-check-circle text-green-500 mr-2"></i>
        <span>Webcast</span>
            </li>

            <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Topics</span>
        </li>

        <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Traning</span>
        </li>

        <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Docs</span>
        </li>

        <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Developer portal</span>
        </li>
        <li className="flex align-items-center mb-4">
            <i className="pi pi-check-circle text-green-500 mr-2"></i>
            <span>Install</span>
        </li>
            </ul>
        </div>


        <div className="cards" style={{  marginBottom: '2em' }}>
        <ul className="list-none">
            <h2>Community</h2>
                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Customers</span>
                </li>
                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Contribute</span>
                </li>
                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Community Programs</span>
                </li>
                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Direction</span>
                </li>
                <li className="flex align-items-center mb-4">
                <i className="pi pi-check-circle text-green-500 mr-2"></i>
                <span>Technology Partners</span>
                </li>

                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Channel Partners</span>
                </li>

                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Open Source Partners</span>
                </li>

                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Dokku for Open Source</span>
                </li>

                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Dokku for startups</span>
                </li>

                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Community</span>
                </li>
            </ul>
        </div>


        <div className="cards" style={{  marginBottom: '2em' }}>
        <ul className="list-none">
                <h2>Support</h2>
            
                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Get Help</span>
                </li>
                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Contact Sales</span>
                </li>
                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Contact Support</span>
                </li>
                <li className="flex align-items-center mb-4">
                    <i className="pi pi-check-circle text-green-500 mr-2"></i>
                    <span>Support Options</span>
                </li>

                <li className="flex align-items-center mb-4">
                            <i className="pi pi-check-circle text-green-500 mr-2"></i>
                            <span>Status</span>
                        </li>
                        <li className="flex align-items-center mb-4">
                            <i className="pi pi-check-circle text-green-500 mr-2"></i>
                            <span>Customers Portal</span>
                        </li>
            </ul>
        </div>

        <div className="cards" style={{  marginBottom: '2em' }}>
        <ul className="list-none">
         <h2>Company</h2>
        
            <li className="flex align-items-center mb-4">
                <i className="pi pi-check-circle text-green-500 mr-2"></i>
                <span>About</span>
            </li>
            <li className="flex align-items-center mb-4">
                <i className="pi pi-check-circle text-green-500 mr-2"></i>
                <span>What is Dokku?</span>
            </li>
            <li className="flex align-items-center mb-4">
                <i className="pi pi-check-circle text-green-500 mr-2"></i>
                <span>Jobs</span>
            </li>
            <li className="flex align-items-center mb-4">
                <i className="pi pi-check-circle text-green-500 mr-2"></i>
                <span>Culture</span>
            </li>

                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Leadership</span>
                    </li>

                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Teams</span>
                    </li>
                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Press</span>
                    </li>
                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Investor Relations</span>
                    </li>
                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Analysts</span>
                    </li>
                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Handbook</span>
                    </li>
                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Security</span>
                    </li>
                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Contact</span>
                    </li>
                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Terms</span>
                    </li>
                    <li className="flex align-items-center mb-4">
                        <i className="pi pi-check-circle text-green-500 mr-2"></i>
                        <span>Privacy</span>
                    </li>
            </ul>
        </div>



          </div>
        
    </Footerpage>
    );
}

export default Footer;
