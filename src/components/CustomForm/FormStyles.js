import styled from 'styled-components';

const FormStylesWrapper = styled.div`
  display: flex;
  flex-direction: column;
    .info_message {
      margin-bottom: 38px;
      font-size: 11px !important;
      margin-top: -8px;
    
    }
  .p-error-custom {
    text-transform: capitalize;
  }
    button.form-button {
      width: 100%;
    }
    .side_message {
      //height: 29px;
      //border-bottom: 1px solid #9ea1ab;
    }
   .login__section {
     i {
       z-index: 10;
     }
     width: 100%;
     .p-password {
       width: 100%;
     }
     input {
       width: 100%;
   }
		.main{
		width: 100%;
		
		h1 {
      font-size: 27px;
      letter-spacing: 0;
      color:  #0b132b;
			}
	form {
		display: flex;
		align-items: flex-start;
		flex-direction: column;
		width: 100%;
		.p-dropdown {
		  width: 100%;
      border-bottom: 1px solid #9ea1ab;
      text-transform: capitalize;
		}
		.p-dropdown-label.p-inputtext.p-placeholder {
		  text-transform: capitalize;
		 }
		.p-dropdown-items-wrapper {
      box-shadow: 0 2px 4px 0 #dfcece;
		}
		.p-dropdown-panel .p-dropdown-item {
          padding: 0.4em .25em;
         :hover {
            background: #ece8e8;
         }
     }
    .p-dropdown-trigger {
        span {
            display: none;
        }
  }
		textarea {
        width: 100%;
        height: 73px;
        border: none;
        border-bottom: 1px solid #9ea1ab;
        resize: none;
        font-size: 15px;
				}
		}
	}
	p ,a {
		font: Light 22px/33px Montserrat;
		letter-spacing: 0.55px;
		margin-top: 2rem;
		margin-bottom: 2.5rem;
		
		span, a {
		text-decoration: none !important;
			cursor: pointer;
			transition: 0.3s ease-in-out;
			:hover {
				opacity: 0.6;
			}
		}
	}
	.forgot {
		display: flex;
		justify-content: space-between;
		width: 100%;
		padding-top: 3em;
		p, a {
			font: Light 22px/33px Montserrat;
			letter-spacing: 0.55px;
			cursor: pointer;
			border-radius: 4px;
			padding: 10px;
			font-size: 13px;
			
			transition: 0.3s ease-in-out;
			:hover {
				background: #44444422;
			}
		}
	}
}
`;

export {FormStylesWrapper};
