/** @format */

import React from 'react';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { FormStylesWrapper } from './FormStyles';
import {Button} from "primereact/button";

const CustomForm = (props) => {
  return (
    <>
      <FormStylesWrapper
        alignItems={props.alignItems}
        inputWidth={props.inputWidth}>
        <div className="main">
          <h1>{props.title}</h1>
          <p className="info-text">{props.infoText}</p>
          <Formik
            enableReinitialize={props.enableReinitialize}
            initialValues={props.initialValues}
            validationSchema={props.initialSchema}
            onSubmit={props.onSubmitFunction}>
            {({ errors, _, touched, setFieldValue }) => (
              <Form>
                {props.formInputs &&
                  props.formInputs.map(
                    ({ name, placeholder, inputType, as, options, icon }) => (
                      <section key={placeholder} className="login__section">
                        <Field
                          onChange={
                            function (e) {
                              setFieldValue(name, e.target?.value || e.value);
                              if (props[name]) {
                                props[name](e.target.value || e.value);
                              }
                            } || function () {}
                          }
                          icon={icon}
                          error={errors}
                          as={as || 'input'}
                          {...(options || null)}
                          placeholder={placeholder}
                          name={name}
                          type={inputType}
                        />{' '}
                        {
                          <ErrorMessage
                            name={name}
                            component="small"
                            className="p-error p-error-custom"
                          />
                        }
                        <span className="side_message">{props[name]}</span>
                        <div className="p-error invalid-feedback info_message">
                          {props.infoTextTwo}
                        </div>
                      </section>
                    )
                  )}
                {props.btnText && (
                  <Button label={!props.loading ? props.btnText : props.btnText || 'please wait...'}  disabled={props.loading || props.disabled} className="form-button"  type="submit" />
                )}
              </Form>
            )}
          </Formik>
        </div>
      </FormStylesWrapper>
    </>
  );
};

export default CustomForm;
