import React from 'react';
import { useQuery} from "@apollo/client";
import getCurrentUser from "./getCurrentUser";
import Login from "../Login/Login";
import LoadingPage from "../Loaders/LoadingPage";

const HandleQuery = ({Component}) => {
  const {loading, error} = useQuery(getCurrentUser)
  return (
     loading ? <LoadingPage/> : error ? <Login/> : <Component/>
  );
};

export default HandleQuery;
