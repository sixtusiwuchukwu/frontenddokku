import React from "react";
import Image from "next/image";
import snippet from "../../assete/severdash/snippet.svg";
import { Snippet } from "./styled.snippet";

const ServerSnippet = () => {
  return (
    <Snippet>
      <div className="surface-0 text-700 text-center mt-8">
        <div className="text-blue-600 font-bold mt-5">
          <Image src={snippet} /> <br />
          <i className="pi pi-discord text-1xl m-3"></i>&nbsp;POWERED BY DOKKU
        </div>
        <div className="text-600 font-bold text-2xl mb-3">
          You don't have starred projects yet.
        </div>
        <div className="text-600 text-1l mb-5">
          Visit a project page and press on a star icon. Then, you can find the
          project on this page.
        </div>
      </div>
    </Snippet>
  );
};

export default ServerSnippet;
