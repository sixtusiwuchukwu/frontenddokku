import React from "react";
import Link from "next/link";
import Image from "next/image";
import create from "../../assete/createnew/create.png";
import template from "../../assete/createnew/template.png";
import swift from "../../assete/createnew/swift.png";
import api from "../../assete/createnew/api.png";
import { CreateNewtab } from "./styled.createnew";

const Createnew = () => {
  return (
    <CreateNewtab>
      <div className="createtab-header-wrapper">
        <h2 className="text-900 font-bold text-4xl mb-4 text-center">
          Create new project
        </h2>

        <div className="grid">
          <a
            href="/createServer/createblankServer"
            className=" col-12 lg:col-4"
          >
            <div
              className=" createtab-container p-4 h-full shadow-2 "
              style={{ borderRadius: "6px", display: "flex" }}
            >
              <div href="/" className=" create-icon flex align-items-center">
                <Image src={create} />
              </div>

              <div className="createtab-text-wrapper">
                <h1 className="createtab-title">Create blank project</h1>
                <p className="createtab-text">
                  Create a blank project to house your files, plan your work,
                  and collaborate on code, among other things.
                </p>
              </div>
            </div>
          </a>

          <a href="/" className=" col-12 lg:col-4">
            <div
              className=" createtab-container p-4 h-full shadow-2 "
              style={{ borderRadius: "6px", display: "flex" }}
            >
              <div className=" create-icon flex align-items-center">
                <Image src={template} />
              </div>

              <div className="createtab-text-wrapper">
                <h1 className="createtab-title">Create from template</h1>
                <p className="createtab-text">
                  Create a project pre-populated with the necessary files to get
                  you started quickly.
                </p>
              </div>
            </div>
          </a>
        </div>
        <div className="grid">
          <a
            href="/createServer/createExternalServer"
            className=" col-12 lg:col-4"
          >
            <div
              className=" createtab-container p-4 h-full shadow-2 "
              style={{ borderRadius: "6px", display: "flex" }}
            >
              <div className=" create-icon flex align-items-center">
                <Image src={api} />
              </div>

              <div className="createtab-text-wrapper">
                <h1 className="createtab-title">
                  Run CI/CD for external repository
                </h1>
                <p className="createtab-text">
                  Connect your external repository to GitLab CI/CD.
                </p>
              </div>
            </div>
          </a>
          <a href="/" className=" col-12 lg:col-4">
            <div
              className=" createtab-container p-4 h-full shadow-2 "
              style={{ borderRadius: "6px", display: "flex" }}
            >
              <div className=" create-icon flex align-items-center">
                <Image src={swift} />
              </div>

              <div className="createtab-text-wrapper">
                <h1 className="createtab-title">Import project</h1>
                <p className="createtab-text">
                  Migrate your data from an external source like GitHub,
                  Bitbucket, or another instance of GitLab.
                </p>
              </div>
            </div>
          </a>
        </div>
        <h4 className="wrapper-text">
          You can also create a project from the command line. Show command
        </h4>
      </div>
    </CreateNewtab>
  );
};

export default Createnew;
