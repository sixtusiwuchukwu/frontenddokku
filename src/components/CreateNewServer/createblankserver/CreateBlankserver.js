// import "primeflex/primeflex.css";
// import MainDash from "../../DashContainers/MainDash";
import { Container } from "./styled.Blankserver";
///importing images
import Image from "next/image";
import create from "../../../assete/createnew/create.png";
import React, { useState } from "react";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Checkbox } from "primereact/checkbox";
import { RadioButton } from "primereact/radiobutton";
import { Dropdown } from "primereact/dropdown";
import { InputTextarea } from "primereact/inputtextarea";

const CreateBlankserver = () => {
  // textarea
  const [textarea, setTextarea] = useState("");
  // dynamic radio button
  const Project_categories = [
    {
      Title: "Private",
      Title_Description:
        "Project access must be granted explicitly to each user. If this project is part of a group, access will be granted to members of the group.",
      key: "p1",
    },
    {
      Title: "Public",
      Title_Description:
        "The project can be accessed without any authentication. ",
      key: "p2",
    },
  ];
  const [selectedCategory, setSelectedCategory] = useState(
    Project_categories[1]
  );

  const [cities1, setCities1] = useState([]);
  const [cities2, setCities2] = useState([]);

  const [selectedState, setSelectedState] = useState(null);

  let states = [
    { name: "Group Servers" },
    { name: "Servers" },
    { name: "Members" },
    { name: "Group-members" },
    { name: "Snippets" },
  ];

  const onCityChange1 = (e) => {
    let selectedCities = [...cities1];

    if (e.checked) selectedCities.push(e.value);
    else selectedCities.splice(selectedCities.indexOf(e.value), 1);

    setCities1(selectedCities);
  };

  const onCityChange2 = (e) => {
    let selectedCities = [...cities2];

    if (e.checked) selectedCities.push(e.value);
    else selectedCities.splice(selectedCities.indexOf(e.value), 1);

    setCities2(selectedCities);
  };

  const onStateChange = (e) => {
    setSelectedState(e.value);
  };
  return (
    <Container>
      <div className="create-server-wrapper">
        <div className="create-server-tabs">
          <div classname="create-server-image" style={{ marginBottom: "2em" }}>
            <Image src={create} alt="devops" className="create-s-image" />
          </div>
          <div className="create-server-title" style={{ marginBottom: "2em" }}>
            <h2>Create New Server</h2>
            <h5>
              Create a blank project to house your files,
              <br /> plan your work, and collaborate on code, among other
              things.
            </h5>
          </div>
        </div>
        <div className="create-server-tab">
          <ul className="list-none p-0 m-5 flex align-items-center font-medium mb-3">
            <li>
              <a className="text-500 no-underline line-height-3 cursor-pointer">
                New Servers
              </a>
            </li>
            <li className="px-2">
              <i className="pi pi-angle-right text-500 line-height-3"></i>
            </li>
            <li>
              <span className="text-900 line-height-3">Create New Server</span>
            </li>
          </ul>

          <div className="p-fluid p-formgrid p-grid">
            <div className="p-field p-col-12 p-md-6">
              <label htmlFor="project name">Project name</label>
              <br />
              <InputText id="project-name" type="text" />
            </div>

            <div className="p-field p-col-12 p-md-6" id="project-Slug">
              <label htmlFor="project Slug">Add Members to Project </label>{" "}
              <br />
              <InputText id="project-slug-input" type="text" />
            </div>

            <div className="p-field p-col-12 p-md-6 ">
              <label htmlFor="description">
                Project description (optional)
              </label>
              <br />
              <InputTextarea
                style={{ padding: "20px" }}
                value={textarea}
                onChange={(e) => setTextarea(e.target.value)}
                placeholder="Description Format"
              />
            </div>
          </div>

          <div className="select-categories-wrapper">
            <h4 className="select-categories-wrapper-title">
              Visibility Level
            </h4>
            {Project_categories.map((category) => {
              return (
                <div
                  key={category.key}
                  className="p-field-radiobutton"
                  style={{ margin: "20px" }}
                >
                  <RadioButton
                    inputId={category.key}
                    name="category"
                    value={category}
                    onChange={(e) => setSelectedCategory(e.value)}
                    checked={selectedCategory.key === category.key}
                  />

                  <label htmlFor={category.key} style={{ margin: "20px" }}>
                    {category.Title}
                  </label>
                  <p style={{ fontSize: "13px" }}>
                    {category.Title_Description}
                  </p>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </Container>
  );
};

export default CreateBlankserver;
