import styled from "styled-components";

export const Container = styled.div`
  position: relative;
  flex-wrap: wrap;
  display: flex;
  justify-content: center;
  justify-content: space-around;
  height: 100%;
  width: 100%;

  .create-server-wrapper {
    display: flex;
    width: 100%;
  }

 
  .create-server-tabs {
    /* margin: auto; */
    flex-wrap: wrap;
    padding:3rem;
    justify-content: space-around;
    width: 100%;
    /* position: relative; */
  }
  .create-server-tab{
    width: 100%;
    /* margin: auto; */
    flex-wrap: wrap;
    padding:3rem;
    justify-content: space-around;
    /* position: relative; */
  }

  .create-s-image {
    width: 60%;
  }
  .create-server-title h2 {
    font-size: 20px;
    font-weight: 600;
    color: #fff;
  }
  .create-server-title h5 {
    font-size: 13px;
    font-weight: 500;
    color: #fff;
  }
  /* FORM STYLING */
  .p-fluid {
    padding: 20px;
    width: 100%;
  }
  /* #project-url,
  #text-area,
  #dropdown,
   */
  label {
    font-weight: 500;
    font-size:14px
  }
  .p-field {
    margin-top: 2rem;
    width: 100%;
  }
  .project {
    display: flex;
    justify-content: space-between;
  }
  /* ------------INPUT STYLING---------------- */
  #project-name {
    /* width: 60%; */
    padding: 8px;
  }
  .projectUrl-wrapper {
    width: 100%;
  }
  .p-inputgroup {
    /* width: 100%; */
  }
  .p-inputgroup-addon {
    /* padding: 3px; */
    font-size:12px
  }
  #dropdown {
    padding: 3px;
    font-Size:13px
  }
  #project-Slug {
    /* margin-left: 3rem; */
  }
  #project-slug-input {
    padding: 8px;
    /* width: 100%; */
  }

  .pi {
    padding: 2px;
  }
  #text-area {
    /* width: 40%; */
  }

  .select-categories-wrapper,
  p,
  label {
    padding: 5px;
  }
  .select-categories-wrapper-title {
    padding: 4px;
    font-weight: 600;
    font-size:15px;
  }
  /* ----------------CreatExternalServer------------ */
  .create-server-title p{
    font-size:13px;
  }
  .documentaries{
    border-radius:8px
    background-color:grey;
    padding:5px;
  }
  .documentaries-ul li{
    margin:12px;
    font-size:13px
  }
  
  /* MEDIA QUERY */
  @media only screen and (max-width: 960px) {
    .create-server-wrapper {
      display: block;
    }
  }
`;
