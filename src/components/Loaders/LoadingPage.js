import React from 'react';
import LoaderImage from '../../assete/loader.gif';
import Image from "next/image";
import styled from "styled-components";
const LoadingPage = () => {
  return (
    <Container>
      <Image height={100} width={100} src={LoaderImage}/>
    </Container>
  );
};
const Container = styled.div`
  display: flex;
  flex: 1;
  height: 100vh;
  width: 100vw;
  justify-content: center;
  align-items: center;
`
export default LoadingPage;
