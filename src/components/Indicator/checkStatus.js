import React from 'react';
import styled from "styled-components";
import colors from "../../defaultTheme/colors";
import PropTypes from 'prop-types';
const CheckStatus = ({status}) => {
  return (
    <>
      {{
        error:<StatusColor style={{backgroundColor:colors.error}}/>,
        success: <StatusColor style={{backgroundColor:colors.success}}/>
      }[status]}
    </>
  );
};
CheckStatus.propTypes = {
  status: PropTypes.oneOf(["success","error"])
};
const StatusColor = styled.span`
    background-color: rgb(231, 28, 28);
    height: 10px;
    width: 10px;
    display: inline-block;
    border-radius: 100px;
`
export default CheckStatus;
