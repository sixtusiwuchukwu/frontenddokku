import React, { useState, useEffect } from "react";
import styled from "styled-components";
import { InputText } from "primereact/inputtext";
import { servers } from "../../../util.js/serverUtil";
// import { servers } from "../../../pages/util.js/server";
import LandingBox from "../SeverBox/LandingBox";

const ServerOverview = () => {
  // let servers = [
  //   {
  //     _id: "1",
  //     serverName: "Dokku Server",
  //     description: "Client and Admin server base still on preview",
  //     active: false,
  //   },
  //   {
  //     _id: "2",
  //     serverName: "Over Flow Server",
  //     description: "Client and Admin server base still on preview",
  //     active: false,
  //   },
  //   {
  //     _id: "3",
  //     serverName: "Deep tech Server",
  //     description: "Client and Admin server base still on preview",
  //     active: false,
  //   },

  //   {
  //     _id: "4",
  //     serverName: "Viz Dev Server",
  //     description: "Client and Admin server base still on preview",
  //     active: false,
  //   },
  //   {
  //     _id: "5",
  //     serverName: "Stack Server",
  //     description: "Client and Admin server base still on preview",
  //     active: false,
  //   },
  //   {
  //     _id: "6",
  //     serverName: "H2H Server",
  //     description: "Client and Admin server base still on preview",
  //     active: false,
  //   },
  //   {
  //     _id: "7",
  //     serverName: "H2H Server",
  //     description: "Client and Admin server base still on preview",
  //     active: false,
  //   },
  //   {
  //     _id: "8",
  //     serverName: "H2H Server",
  //     description: "Client and Admin server base still on preview",
  //     active: false,
  //   },
  // ];

  // const list = ['s','s','s','s','s','s','s','s','sd','sd','s','r'].map((data)=> <LandingBox  key={data}/>)

  let list = servers.map((data) => <LandingBox dataCards={data} />);

  return (
    <Outer style={{ display: "block" }}>
      <div className="server-wrapper">
        <ul className="list-none p-0 m-0 flex align-items-center font-medium mb-3">
          <li style={{ fontSize: "13px", margin: "1rem" }}>
            <a className="text-500 no-underline line-height-3 cursor-pointer">
              Servers
            </a>
          </li>
          <li className="px-2">
            <i className="pi pi-angle-right text-500 line-height-3"></i>
          </li>
          <li>
            <span
              className="text-900 line-height-3"
              style={{ fontSize: "13px", margin: "1rem" }}
            >
              Analytics
            </span>
          </li>
          <span className="p-input-icon-right " style={{}}>
            <i className="pi pi-search" />
            <InputText
              onChange={(e) => setInput(e.target.value)}
              placeholder="Search"
              style={{ padding: "5px", width: "100%", borderRadius: "8px" }}
            />
          </span>

          {/* <div className="output">
            {output.map((item) => (
              <p>{item.servers}</p>
            ))}
          </div> */}
        </ul>
      </div>

      <Container>{list}</Container>
    </Outer>
  );
};

const Outer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
`;
const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: auto;

  .container {
    content: "";
    flex: auto;
  }
  .tabs {
    margin: auto;
    display: flex;
    flex-wrap: wrap;
    margin: 20px;
    justify-content: space-around;
  }
`;

export default ServerOverview;
