import React from 'react';
import styled from "styled-components";
import Logout from "../IsAuth/Logout";

const NavTop = () => {
  return (
    <Container>
      <Label>Dokku Dash</Label>
      <Logout/>
    </Container>
  );
};

const Label = styled.div`
  font-size: 20px;
  font-weight: bolder;
`

const Container = styled.div`
  justify-content: space-between;
  display: flex;
  height: 3em;
  padding: 10px;
  border-bottom: 1px solid #647b9b36;
`
export default NavTop;
