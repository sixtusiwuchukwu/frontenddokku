import React from 'react';
import styled from "styled-components";
import { FaPowerOff } from 'react-icons/fa';


const Logout = () => {
  return (
    <Container>
      <div className="logout"><FaPowerOff/> Logout</div>
    </Container>
  );
};

const Container = styled.div`
svg {
  font-size: 18px;
  top: 4px;
  position: relative;
  color:#fff;
}
`
export default Logout;
