import classNames from "classnames";
import logo from "../src/assete/Dlogo.png";
import Image from "next/image";
import { Nav } from "../styles/styled.Apptop";
import React, { useRef, useState } from "react";
import { InputText } from "primereact/inputtext";
import { SplitButton } from "primereact/splitbutton";
import { Toast } from "primereact/toast";
import { SiGit } from "react-icons/si";
import { BiGitPullRequest } from "react-icons/bi";
import { FiCheckCircle } from "react-icons/fi";
import { BsQuestionDiamondFill } from "react-icons/bs";
import { Menubar } from "primereact/menubar";
import { Link } from "react-router-dom";

export const AppTopbar = (props) => {
  // drop down list
  const toast = useRef(null);

  const save = () => {
    window.location.href = "/";
  };
  // Search input
  // const [value, setValue] = useState("");

  ///dropdown menu

  const Newrepo = [
    {
      label: "New Repo",
      icon: "pi pi-fw pi-plus",
      items: [
        {
          label: "New Server",
          icon: "pi pi-fw pi-plus",
          command: () => {
            window.location.href = "/createNewserver";
          },
        },
        {
          label: "New Group Server",
          icon: "pi pi-external-link",
        },
        {
          label: "Snippet",
          icon: "pi pi-fw pi-align-center",
        },
        {
          label: "Add Server",
          icon: "pi pi-upload",
          command: () => {
            window.location.hash = "/fileupload";
          },
        },
      ],
    },
  ];

  return (
    <Nav>
      {/* <Toast ref={toast}></Toast> */}
      <div className="layout-topbar">
        <a href="/" className="layout-topbar-logo ">
          <Image src={logo} alt="logo" />
          <span style={{ fontSize: "15px", fontWeight: "600" }}>
            {" "}
            Dokku Dash
          </span>
        </a>

        <button
          type="button"
          className="p-link  layout-menu-button layout-topbar-button"
          onClick={props.onToggleMenuClick}
        >
          <i className="pi pi-bars" />
        </button>

        <button
          type="button"
          className="p-link layout-topbar-menu-button layout-topbar-button"
          onClick={props.onMobileTopbarMenuClick}
        >
          <i className="pi pi-ellipsis-v" />
        </button>

        <ul
          className={classNames("layout-topbar-menu lg:flex origin-top", {
            "layout-topbar-menu-mobile-active": props.mobileTopbarMenuActive,
          })}
        >
          <li>
            <Menubar
              model={Newrepo}
              onClick={save}
              style={{ border: "none", fontSize: "11px" }}
            />
          </li>

          <li>
            <button
              className="p-link layout-topbar-button"
              onClick={props.onMobileSubTopbarMenuClick}
              model={Newrepo}
            >
              <SiGit style={{ fontSize: "15px" }} />
              <span>Issues</span>
            </button>
          </li>
          <li>
            <button
              className="p-link layout-topbar-button"
              onClick={props.onMobileSubTopbarMenuClick}
            >
              <BiGitPullRequest style={{ fontSize: "15px" }} />
              <span>Events</span>
            </button>
          </li>

          <li>
            <button
              className="p-link layout-topbar-button"
              onClick={props.onMobileSubTopbarMenuClick}
            >
              <FiCheckCircle style={{ fontSize: "15px" }} />
              <span>Events</span>
            </button>
          </li>

          <li>
            <button
              className="p-link layout-topbar-button"
              onClick={props.onMobileSubTopbarMenuClick}
            >
              <BsQuestionDiamondFill style={{ fontSize: "15px" }} />
            </button>
          </li>

          <li className="dropdown">
            <button
              className="p-link layout-topbar-button dropbtn"
              onClick={props.onMobileSubTopbarMenuClick}
            >
              <i className="pi pi-user" />
            </button>
            <div className="dropdown-content">
              <a href="/">Profile</a>
              <a href="#">setting</a>
              <a href="#">Log out</a>
            </div>
          </li>
        </ul>
      </div>
    </Nav>
  );
};
