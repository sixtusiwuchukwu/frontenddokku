import React from 'react';
import {favicon} from '../public/favicon.ico'

export const AppFooter = (props) => {

    return (

        <div>

        <img src="/favicon.ico" alt='favicon' style={{width:"30px",margin:"5px"}}/>
        <span className=" ml-2" style={{fontWeight:"700"}}>DOKKU SERVER</span>
        
        </div>
            );


}
// font-medium