import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import ServerDash from "../../src/components/DashContainers/ServerDash";
// import Servers from "../../src/components/ServerDashboard/Servers";
import { getSpecifiedServer, servers } from "../../util.js/serverUtil";
import CheckLogin from "../../src/components/AuthControl/CheckLogin";

 function Index() {
  const router = useRouter();
  const { query } = router.query;
  const [server, setServer] = useState([]);

  useEffect(() => {
    getSpecifiedServer({ server: servers, id: query, setServer });
  }, []);

  const __foundserver = server && (
    <div>
      <h2>{server.serverName}</h2>
      <span>{server._id}</span>
    </div>
  );
  return <ServerDash>Server: {__foundserver}</ServerDash>;
}

const DashBoard = () => {
   return <CheckLogin Component={Index}/>
}

export default DashBoard
