import React from "react";
import ForgotPassword from "../src/components/ForgotPassword/Forgotpassword";
import CheckAuthForUnAuthPages from "../src/components/AuthControl/checkAuthForUnAuthPages";

export default function ForgotCode() {
  return (
    <CheckAuthForUnAuthPages>
      <ForgotPassword />
    </CheckAuthForUnAuthPages>
  );
}
