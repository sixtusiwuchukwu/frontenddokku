import React from 'react';
import LoginPage from "../src/components/Login/Login";
import CheckAuthForUnAuthPages from "../src/components/AuthControl/checkAuthForUnAuthPages";

const Login = () => {
  return (
    <CheckAuthForUnAuthPages>
      <LoginPage/>
    </CheckAuthForUnAuthPages>
  );
};

export default Login;
