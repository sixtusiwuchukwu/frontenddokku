// noinspection HtmlUnknownTarget

import React  from "react";
import Link from "next/link";
import Image from "next/image";
import logo from "../src/assete/logo.png";
import {Signup} from "../src/components/SignUp/styled.SignUp";
import CustomForm from "../src/components/CustomForm/Form";
import * as Yup from "yup";
import TextInput from "../src/components/Input/TextInput";
import PasswordInput from "../src/components/Input/PasswordInput";
import {gql, useMutation} from "@apollo/client";
import CheckAuthForUnAuthPages from "../src/components/AuthControl/checkAuthForUnAuthPages";

const addUserMutation = gql`

    mutation addUser ($email:EmailAddress! $password:String!){
        addUser(data:{
            email: $email
            password: $password
        })
    }
`

const initialSchemaAccountInformation = Yup.object().shape({
  email: Yup.string().required().email("Invalid email address. E.g. example@email.com"),
  password: Yup.string().required().min(4)
});
const AccountInformationInput = [
  {
    placeholder: 'Email*',
    name: 'email',
    inputType: 'email',
    as: TextInput,
    icon:"pi-envelope"
  },
  {
    placeholder: 'Password*',
    name: 'password',
    inputType: 'password',
    as: PasswordInput,
    options: {feedback:false,toggleMask:true}
  },
];

const accountInformation = {
  email: '',
  password: ''
};

const SignUp = () => {
  const [addUser, {loading}] = useMutation(addUserMutation)
  return (
    <CheckAuthForUnAuthPages>
    <Signup>
      <div className=" Signup-wrapper">
        <h2 className="signup-header-h2">DOKKU.COM </h2>
        <h5 className="signup-header-h5" style={{ lineHeight: "1.5" }}>
          Dokku.com offers free unlimited (private) repositories and unlimited
          collaborators.
        </h5>
        <ul className="signup-list" style={{ lineHeight: "2.5" }}>
          <li className="signup-list-item">
            Explore projects on Dokku.com (no login needed)
          </li>
          <li className="signup-list-item">More information about Dokku.com</li>
          <li className="signup-list-item">Dokku Community Forum</li>
          <li className="signup-list-item">
            <Link href="/">Dokku Homepage</Link>
          </li>
          
          <h5 className="signup-header-h5" style={{ lineHeight: "1.5" }}>
            By signing up for and by signing in to this service you accept our:
          </h5>
          
          <li className="signup-list-item">Privacy policy</li>
          <li className="signup-list-item">Dokku.com Terms.</li>
        </ul>
      </div>
      
      <div className="form-auth">
        <div className="p-d-flex p-jc-center">
          <div className="card">
            <div className="logo-container">
              <div className="login-logo-wrapper">
                <Image src={logo} className="login-logo" />
              </div>
            </div>
            <h5 className="p-text-center-dash">Sign Up</h5>
            <CustomForm
              alignItems="flex-end !important"
              inputWidth="100%"
              onSubmitFunction={(fields) => {
                addUser({variables:fields}).catch(()=>{})
              }}
              initialSchema={initialSchemaAccountInformation}
              initialValues={accountInformation}
              title=""
              infoText=""
              btnText={loading ? 'please wait' : 'Sign Up'}
              loading={loading}
              formInputs={AccountInformationInput}
            />
            <div className="p-field">
              <ul className="Login-wrapper">
                <li
                  className="Login-text-wrapper"
                  style={{ padding: "10px" }}
                >
                    <span>
                      Already have an Account yet?
                      <Link href="/login">Sign In</Link>
                    </span>
                </li>
              </ul>
            </div>
            <p style={{ fontSize: "12px", marginTop:20, marginBlock:20 }}>
              By clicking Register, I agree that I have read and accepted the{" "}
              <br />
              GitLab <Link href="/">Terms of Use and Privacy Policy</Link>
            </p>
          </div>
        </div>
      </div>
    </Signup>
    </CheckAuthForUnAuthPages>
  );
};

export default SignUp;
