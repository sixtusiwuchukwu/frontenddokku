import Head from 'next/head';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import 'prismjs/themes/prism-tomorrow.css';
import '../src/layout/flags/flags.css';
import '../src/layout/layout.scss';
import '../src/App.scss';
import '../src/themes/vela-blue/theme.css';
import React from 'react';
import Apollo from '../util.js/apollo'
import NextNProgress from 'nextjs-progressbar';
import PrimeReact from 'primereact/api';
PrimeReact.ripple = true;

function MyApp({ Component, pageProps }) {
    return (
        <>
          <Head>
            <title>DPM</title>
            <meta charSet="utf-8" />
            <link rel="icon" href="/favicon.ico?3=3" />
            <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png?la=3" />
            <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?d=3" />
            <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?d=4" />
            <meta name="theme-color" content="#ffffff" />
            <meta name="viewport" content="initial-scale=1.0, width=device-width" />
            <meta name="description" content="Web site created using create-next-app" />
            <meta name="theme-color" content="#000000" />
          </Head>
          <NextNProgress
            color="#29D"
            startPosition={0.3}
            stopDelayMs={200}
            height={3}
            showOnShallow={true}
          />
          <Component {...pageProps} />
        </>
    )
}
const MainContainer = props => {
  return <Apollo><MyApp {...props}/></Apollo>
}
export default MainContainer;
